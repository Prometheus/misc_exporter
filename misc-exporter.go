package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const (
	symlinkFormat   = `misc_symlink{path="%s", path_base="%s", target="%s", target_base="%s", target_real="%s"} 1` + "\n"
	lvLinkFormat    = `misc_disk_lv{vg="%s", lv="%s", device="%s"} 1` + "\n"
	diskLabelFormat = `misc_disk_by_label{label="%s", device="%s"} 1` + "\n"
	diskIDFormat    = `misc_disk_by_id{id="%s", device="%s"} 1` + "\n"
)

var (
	Symlinks       []string
	LVLinks        []string
	DiskLabelLinks []string
	DiskIDLinks    []string
)

type Link struct {
	Path, Target string
}

type LinkHandler func(link Link, w io.Writer)

func (l Link) Abs() (abs string) {
	abs = l.Target
	if !filepath.IsAbs(l.Target) {
		abs, _ = filepath.Abs(filepath.Join(filepath.Dir(l.Path), l.Target))
	}
	return abs
}

func resolveLink(path string) (link Link, err error) {
	link.Path = path
	link.Target, err = os.Readlink(path)
	return
}

func writeLinks(links []string, handler LinkHandler, w io.Writer) {
	for _, glob := range links {
		links, _ := filepath.Glob(glob)
		for _, link := range links {
			link, err := resolveLink(link)
			if err == nil {
				handler(link, w)
			}
		}
	}
}

func writeSymlink(link Link, w io.Writer) {
	fmt.Fprintf(w, symlinkFormat, link.Path, filepath.Base(link.Path), link.Target, filepath.Base(link.Target), link.Abs())
}

func writeLVLink(link Link, w io.Writer) {
	fmt.Fprintf(w, lvLinkFormat, filepath.Base(filepath.Dir(link.Path)), filepath.Base(link.Path), filepath.Base(link.Target))
}

func writeDiskLabelLink(link Link, w io.Writer) {
	fmt.Fprintf(w, diskLabelFormat, filepath.Base(link.Path), filepath.Base(link.Target))
}

func writeDiskIDLink(link Link, w io.Writer) {
	fmt.Fprintf(w, diskIDFormat, filepath.Base(link.Path), filepath.Base(link.Target))
}

func metricsHandler(w http.ResponseWriter, r *http.Request) {
	writeLinks(Symlinks, writeSymlink, w)
	writeLinks(LVLinks, writeLVLink, w)
	writeLinks(DiskLabelLinks, writeDiskLabelLink, w)
	writeLinks(DiskIDLinks, writeDiskIDLink, w)
}

func main() {
	var links, lvs, diskLabels, diskIDs string
	var addr string

	flag.StringVar(&links, "links", "", "Patterns for symlinks, separated by ;")
	flag.StringVar(&diskLabels, "disk-labels", "/dev/disk/by-label/*", "Patterns for disks by label, separated by ;")
	flag.StringVar(&diskIDs, "disk-ids", "/dev/disk/by-id/*", "Patterns for disks by ID, separated by ;")
	flag.StringVar(&lvs, "lvs", "/dev/*-vg/*;/dev/vg-*/*", "Patterns for logical volumes, separated by ;")
	flag.StringVar(&addr, "addr", ":9143", "Address and port to listen on")
	flag.Parse()

	Symlinks = strings.Split(links, ";")
	LVLinks = strings.Split(lvs, ";")
	DiskLabelLinks = strings.Split(diskLabels, ";")
	DiskIDLinks = strings.Split(diskIDs, ";")

	fmt.Println("Listening on ", addr)
	http.HandleFunc("/metrics", metricsHandler)
	http.ListenAndServe(addr, nil)
}
