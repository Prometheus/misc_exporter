# Prometheus misc exporter
Exports miscellaneous information from the system.

Metrics are:

- `misc_symlink` symlink information that can be used for resolving devices.
